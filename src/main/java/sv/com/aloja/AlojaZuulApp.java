package sv.com.aloja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class AlojaZuulApp {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(AlojaZuulApp.class, args);
	}

}
